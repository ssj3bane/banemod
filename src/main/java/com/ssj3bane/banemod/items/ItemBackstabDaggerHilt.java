package com.ssj3bane.banemod.items;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemBackstabDaggerHilt extends Item {
	public static Object[] recipe = new Object[] {
		"sis",
		" s ",
		's', Items.stick,
		'i', Items.iron_ingot
	};
	public ItemBackstabDaggerHilt() {
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabMaterials);
		setUnlocalizedName("itemBackstabDaggerHilt");
		setTextureName(BaneMod.MODID + ":itemBackstabDaggerHilt");
	}
	
	public static ItemBackstabDaggerHilt create() {
		ItemBackstabDaggerHilt result = new ItemBackstabDaggerHilt();
		GameRegistry.registerItem(result, "itemBackstabDaggerHilt");
		GameRegistry.addShapedRecipe(new ItemStack(result), recipe);
		return result;
	}
}