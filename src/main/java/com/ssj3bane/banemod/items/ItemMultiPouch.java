package com.ssj3bane.banemod.items;

import java.util.List;

import com.ssj3bane.banemod.BaneMod;
import com.ssj3bane.banemod.util.BaneUtil;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.StatCollector;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;

public class ItemMultiPouch extends Item
{
	public static Object[] recipe = new Object[] {
		"LGL",
		"LEL",
		"ILI",
		'L', Items.leather,
		'G', Items.gold_ingot,
		'E', Items.ender_pearl,
		'I', Items.iron_ingot
	};
	public static String unlocalizedName = "itemMultiPouch";
	public static NBTTagCompound nbt;
	public ItemMultiPouch() {
		setMaxStackSize(1);
//		setCreativeTab(BaneMod.tabName);
		setUnlocalizedName(unlocalizedName);
		setTextureName(BaneMod.MODID + ":itemMultiPouch");
	}
//	@SideOnly(Side.CLIENT)
//	private IIcon icon0;
//
//	@SideOnly(Side.CLIENT)
//	public IIcon getIcon(ItemStack stack, int renderPass)
//	{
//		return getIconFromDamageForRenderPass(stack.getItemDamage(), renderPass);
//	}
//
//    @SideOnly(Side.CLIENT)
//    public void registerIcons(IIconRegister iconRegister)
//    {
//        this.icon0 = iconRegister.registerIcon(BaneMod.MODID + ":" + "ItemMultiPouch");
//    }
	public static ItemMultiPouch create() {
		ItemMultiPouch result = new ItemMultiPouch();
		GameRegistry.registerItem(result, ItemMultiPouch.unlocalizedName);
		return result;
	}
	public static void setupNBT(ItemStack stack) {
		if (stack.hasTagCompound()) {
			nbt = stack.getTagCompound();
		} else {
			NBTTagInt slotSelected = new NBTTagInt(0);
			NBTTagList list = new NBTTagList();
			nbt = new NBTTagCompound();
			nbt.setTag("ItemInventory", list);
			nbt.setTag("SlotSelected", slotSelected);
		}
	}
	public static NBTTagCompound setupNewNBT(ItemStack stack) {
		NBTTagCompound _nbt = new NBTTagCompound();
		if (stack.hasTagCompound()) {
			_nbt = stack.getTagCompound();
		} else {
			NBTTagInt slotSelected = new NBTTagInt(0);
			NBTTagList list = new NBTTagList();
			_nbt = new NBTTagCompound();
			_nbt.setTag("ItemInventory", list);
			_nbt.setTag("SlotSelected", slotSelected);
		}
		return _nbt;
	}
	public static void changeSelectedSlot(ItemStack stack, int dWheel) {
		setupNBT(stack);
    	int inventoryLength = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).tagCount() - 1;
    	int slotSelected = nbt.getInteger("SlotSelected");
		NBTTagInt newSlot = new NBTTagInt(slotSelected);
		if(dWheel < 0) {
			// Go back a slot
			if(slotSelected - 1 < 0) {
				//Loop to the last slot
				newSlot = new NBTTagInt(inventoryLength);
			} else {
				//Go back a slot normally
				newSlot = new NBTTagInt(slotSelected - 1);
			}
		} else if(dWheel > 0) {
			//Go forward a slot
			if(slotSelected + 1 > inventoryLength) {
				//Loop to the first slot
				newSlot = new NBTTagInt(0);
			} else {
				//Go forward a slot normally
				newSlot = new NBTTagInt(slotSelected + 1);
			}
		}
		nbt.setTag("SlotSelected", newSlot);
		NBTTagCompound tempItemNBT = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).getCompoundTagAt(nbt.getInteger("SlotSelected"));
		ItemStack tempItem = ItemStack.loadItemStackFromNBT(tempItemNBT);
		adjustNBTDisplay(stack, tempItem);
    	stack.setTagCompound(nbt);
	}
    @Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {
		setupNBT(itemStack);
    	itemStack.setTagCompound(nbt);
		if(entityPlayer.isSneaking()) {
			entityPlayer.openGui(BaneMod.instance, BaneMod.GUI_ENUM.MULTIPOUCH.ordinal(), world, 0, 0, 0);
		} else {
			int slotSelected = nbt.getInteger("SlotSelected");
			NBTTagCompound tempItemNBT = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).getCompoundTagAt(slotSelected);
			ItemStack tempItem = ItemStack.loadItemStackFromNBT(tempItemNBT);
			if(tempItem != null) {
				String tempItemModName = Item.itemRegistry.getNameForObject(tempItem.getItem());
				boolean inefficientCheck = BaneUtil.isItemBanned(tempItemModName);
				if(!inefficientCheck) {
					tempItem.useItemRightClick(world, entityPlayer);
					if(tempItem.stackSize > 0) {
						tempItem.writeToNBT(nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).getCompoundTagAt(slotSelected));
					} else {
						nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).removeTag(slotSelected);
					}
				}
			}
		}
		return itemStack;
	}
    
    public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par3) {
		setupNBT(itemStack);
    	int inventoryLength = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).tagCount() - 1;
    	int slotSelected = nbt.getInteger("SlotSelected");
    	NBTTagCompound tempItemNBT = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).getCompoundTagAt(slotSelected);
		ItemStack tempItem = ItemStack.loadItemStackFromNBT(tempItemNBT);
		
		if(!GuiScreen.isShiftKeyDown()) {
			list.add("Multi Pouch");
			if(tempItem != null) {
				list.add("Item selected: " + tempItem.getDisplayName());
			} else {
				list.add("Item selected: None");
			}
			list.add("Press shift to view all items");
		} else {
			list.add("Multi Pouch");
			if(tempItem != null) {
				list.add("Item selected: " + tempItem.getDisplayName());
			} else {
				list.add("Item selected: None");
			}
			for(int ind = 0; ind <= inventoryLength; ind++) {
				NBTTagCompound itemNBT = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).getCompoundTagAt(ind);
				ItemStack loadedItem = ItemStack.loadItemStackFromNBT(itemNBT);
				list.add("Slot " + ind + ": " + loadedItem.getDisplayName());
			}
		}
	}
	public static void setSlotSelected(ItemStack stack, int slotSelected) {
		setupNBT(stack);
		nbt.setTag("SlotSelected", new NBTTagInt(slotSelected));
		NBTTagCompound tempItemNBT = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).getCompoundTagAt(nbt.getInteger("SlotSelected"));
		ItemStack tempItem = ItemStack.loadItemStackFromNBT(tempItemNBT);
		adjustNBTDisplay(stack, tempItem);
		stack.setTagCompound(nbt);
	}
	public static void adjustNBTDisplay(ItemStack stack, ItemStack mimicItem) {
		if(mimicItem != null) {
			String itemName = Item.itemRegistry.getNameForObject(mimicItem.getItem());
			boolean inefficientCheck = BaneUtil.isItemBanned(itemName);
			if(!inefficientCheck) {
				if(nbt.hasKey("display", 10)) {
					nbt.getCompoundTag("display").setString("Name",(""+StatCollector.translateToLocal(stack.getUnlocalizedName() + ".name")).trim() + " - " + mimicItem.getDisplayName());
				} else {
					nbt.setTag("display", new NBTTagCompound());
					nbt.getCompoundTag("display").setString("Name",(""+StatCollector.translateToLocal(stack.getUnlocalizedName() + ".name")).trim() + " - " + mimicItem.getDisplayName());
				}
			} else {
				if(nbt.hasKey("display", 10)) {
					nbt.getCompoundTag("display").setString("Name",(""+StatCollector.translateToLocal(stack.getUnlocalizedName() + ".name")).trim() + " - Banned Item");
				} else {
					nbt.setTag("display", new NBTTagCompound());
					nbt.getCompoundTag("display").setString("Name",(""+StatCollector.translateToLocal(stack.getUnlocalizedName() + ".name")).trim() + " - Banned Item");
				}
			}
		} else {
			if(nbt.hasKey("display", 10)) {
				nbt.getCompoundTag("display").setString("Name",(""+StatCollector.translateToLocal(stack.getUnlocalizedName() + ".name")).trim() + " - None");
			} else {
				nbt.setTag("display", new NBTTagCompound());
				nbt.getCompoundTag("display").setString("Name",(""+StatCollector.translateToLocal(stack.getUnlocalizedName() + ".name")).trim() + " - None");
			}
		}
	}
}