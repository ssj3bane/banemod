package com.ssj3bane.banemod.items;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMedicalPlant extends Item {
	public ItemMedicalPlant() {
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabMaterials);
		setUnlocalizedName("itemMedicalPlant");
		setTextureName(BaneMod.MODID + ":itemMedicalPlant");
	}
	
	public static ItemMedicalPlant create() {
		ItemMedicalPlant result = new ItemMedicalPlant();
		GameRegistry.registerItem(result, "itemMedicalPlant");
		return result;
	}
}
