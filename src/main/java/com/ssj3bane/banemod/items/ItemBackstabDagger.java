package com.ssj3bane.banemod.items;

import java.awt.List;
import java.util.Random;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.ssj3bane.banemod.BaneMod;
import com.ssj3bane.banemod.util.BaneUtil;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class ItemBackstabDagger extends ItemSword {
	public Object[] recipe;
	public float damageValue;
	public int maxDamage;
	public String unlocalizedName;
	public double backstabMultiplier;
	public ToolMaterial toolMaterial;
	public ItemStack[] repairList;
	public static final float backstabThreshold = 30.0F;
	public ItemBackstabDagger(String _unlocalizedName, Object[] _recipe, float _damageValue, int _maxDamage, double _backstabMultiplier, ItemStack[] _repairList) {
		super(ToolMaterial.EMERALD);
		this.toolMaterial = ToolMaterial.EMERALD;
		this.repairList = _repairList;
		setMaxStackSize(1);
		setCreativeTab(CreativeTabs.tabCombat);
		this.unlocalizedName = _unlocalizedName;
		this.recipe = _recipe;
		this.damageValue = _damageValue;
		this.maxDamage = _maxDamage;
		this.backstabMultiplier = _backstabMultiplier;
		setHasSubtypes(true);
		setMaxDamage(_maxDamage);
		setUnlocalizedName(unlocalizedName);
		setTextureName(BaneMod.MODID + ":" + _unlocalizedName);
	}

	public static ItemBackstabDagger create(String _unlocalizedName, Object[] _recipe, float _damageValue, int _maxDamage, double _backstabMultiplier, ItemStack[] _repairList) {
		ItemBackstabDagger result = new ItemBackstabDagger(_unlocalizedName, _recipe, _damageValue, _maxDamage, _backstabMultiplier, _repairList);
		GameRegistry.registerItem(result, result.unlocalizedName);
		GameRegistry.addShapedRecipe(new ItemStack(result), result.recipe);
		return result;
	}
	
	public static ItemBackstabDagger createShapeless(String _unlocalizedName, Object[] _recipe, float _damageValue, int _maxDamage, double _backstabMultiplier, ItemStack[] _repairList) {
		ItemBackstabDagger result = new ItemBackstabDagger(_unlocalizedName, _recipe, _damageValue, _maxDamage, _backstabMultiplier, _repairList);
		GameRegistry.registerItem(result, result.unlocalizedName);
		GameRegistry.addShapelessRecipe(new ItemStack(result), result.recipe);
		return result;
	}
	
	/**
     * Called when the player Left Clicks (attacks) an entity.
     * Processed before damage is done, if return value is true further processing is canceled
     * and the entity is not attacked.
     *
     * @param stack The Item being used
     * @param player The player that is attacking
     * @param entity The entity being attacked
     * @return True to cancel the rest of the interaction.
     */
	@Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
		boolean backstabbed = performBackStab((EntityLivingBase) player, (EntityLivingBase) entity);
		if(backstabbed) {
			entity.attackEntityFrom(DamageSource.causePlayerDamage(player), (float) (this.damageValue * this.backstabMultiplier));
			stack.attemptDamageItem(1, new Random());
		} else {
			entity.attackEntityFrom(DamageSource.causePlayerDamage(player), (float) this.damageValue);
			stack.attemptDamageItem(2, new Random());
		}
		return true;
    }
	
	public boolean isItemTool(ItemStack p_77616_1_) {
		return true;
    }
	
	/*
	 * Assumed to be what determined +# attack damage
	 */
	@Override
	public float func_150931_i() {
        return (float) this.damageValue;
    }
	
	public void addInformation(ItemStack stack, EntityPlayer player, List itemInformation, boolean par4) {
		itemInformation.add("Has the potential to do backstab damage");
		itemInformation.add("");
		itemInformation.add("+" + (this.damageValue * this.backstabMultiplier) + " backstab damage");
	}
	
	/**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
	@Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {
		/*
		 * Eventually, will add throwing
		 */
        return itemStack;
    }
	
	/**
     * Determines if the durability bar should be rendered for this item.
     * Defaults to vanilla stack.isDamaged behavior.
     * But modders can use this for any data they wish.
     *
     * @param stack The current Item Stack
     * @return True if it should render the 'durability' bar.
     */
    public boolean showDurabilityBar(ItemStack stack) {
        return stack.isItemDamaged();
    }

    /**
     * Queries the percentage of the 'Durability' bar that should be drawn.
     *
     * @param stack The current ItemStack
     * @return 1.0 for 100% 0 for 0%
     */
    public double getDurabilityForDisplay(ItemStack stack) {
        return (double)stack.getItemDamageForDisplay() / (double)stack.getMaxDamage();
    }

    /**
     * Return the enchantability factor of the item, most of the time is based on material.
     */
    public int getItemEnchantability() {
        return this.toolMaterial.getEnchantability();
    }

    /**
     * Return whether this item is repairable in an anvil.
     */
    public boolean getIsRepairable(ItemStack itemStack, ItemStack itemStack2) {
        return BaneUtil.contains(this.repairList, itemStack2);
    }

    /**
     * Gets a map of item attribute modifiers, used by ItemSword to increase hit damage.
     */
	public Multimap getItemAttributeModifiers() {
        Multimap multimap = HashMultimap.create();
        multimap.put(SharedMonsterAttributes.attackDamage.getAttributeUnlocalizedName(), new AttributeModifier(field_111210_e, "Weapon modifier", (double)this.damageValue + 4.0F, 0));
        multimap.put("backstabDamage", new AttributeModifier(field_111210_e, "Weapon modifier", (double)((this.damageValue + 4.0F) * this.backstabMultiplier), 0));
        return multimap;
    }
	public boolean performBackStab(EntityLivingBase entityHitting, EntityLivingBase entityHit) {
		double targetYaw = entityHit.getRotationYawHead() % 360.0F;
	    double playerYaw = entityHitting.getRotationYawHead() % 360.0F;
	    if(playerYaw < 0.0D) playerYaw += 360.0D;
	    if(targetYaw < 0.0D) targetYaw += 360.0D;
	    if((Math.abs(targetYaw - playerYaw) < backstabThreshold) || (360.0D - Math.abs(targetYaw - playerYaw) < backstabThreshold)) {
			return true;
	    }
	    return false;
	}
}