package com.ssj3bane.banemod.items;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemMedicalPlantProduct extends ItemFood {
	private boolean alwaysEdible = true;
    private final int healAmount = 10;
	public static Object[] recipe = new Object[] {
		Items.bowl, BaneMod.itemMedicalPlant
	};
	public ItemMedicalPlantProduct() {
		super(2, 0, false);
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabBrewing);
		setUnlocalizedName("itemMedicalPlantProduct");
		setTextureName(BaneMod.MODID + ":itemMedicalPlantProduct");
		
	}
	
	public static ItemMedicalPlantProduct create() {
		ItemMedicalPlantProduct result = new ItemMedicalPlantProduct();
		GameRegistry.registerItem(result, "itemMedicalPlantProduct");
		GameRegistry.addShapelessRecipe(new ItemStack(result), recipe);
		return result;
	}

    public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player) {
        --stack.stackSize;
        player.getFoodStats().func_151686_a(this, stack);
        world.playSoundAtEntity(player, "random.burp", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
        this.onFoodEaten(stack, world, player);
        return stack;
    }

    protected void onFoodEaten(ItemStack stack, World world, EntityPlayer player) {
        player.heal(this.healAmount);
    }

    /**
     * How long it takes to use or consume an item
     */
    public int getMaxItemUseDuration(ItemStack stack) {
        return 32;
    }

    /**
     * returns the action that specifies what animation to play when the items is being used
     */
    public EnumAction getItemUseAction(ItemStack stack) {
        return EnumAction.eat;
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        if (player.canEat(this.alwaysEdible)) {
            player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        }

        return stack;
    }
}
