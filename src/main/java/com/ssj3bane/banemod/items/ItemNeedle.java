package com.ssj3bane.banemod.items;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemNeedle extends Item {
	public ItemNeedle() {
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("itemNeedle");
		setTextureName(BaneMod.MODID + ":itemNeedle");
	}
	
	public static ItemNeedle create() {
		ItemNeedle result = new ItemNeedle();
		GameRegistry.registerItem(result, "itemNeedle");
		return result;
	}
}
