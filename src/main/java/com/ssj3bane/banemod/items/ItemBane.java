package com.ssj3bane.banemod.items;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemBane extends Item
{
	public static String unlocalizedName = "itemBane";
	public static NBTTagCompound nbt;
	public ItemBane() {
		setMaxStackSize(1);
		setCreativeTab(null);
		setUnlocalizedName(unlocalizedName);
		setTextureName(BaneMod.MODID + ":" + unlocalizedName);
	}
	public static ItemBane create() {
		ItemBane result = new ItemBane();
		GameRegistry.registerItem(result, ItemBane.unlocalizedName);
		return result;
	}
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister IIconRegister) {
		itemIcon = IIconRegister.registerIcon("banemod:itemBane");
	}
	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {
		if (itemStack.hasTagCompound()) {
			nbt = itemStack.getTagCompound();
		} else {
			nbt = new NBTTagCompound();
		}
		nbt.setString("OwnerUUID", entityPlayer.getUniqueID().toString());
		itemStack.setTagCompound(nbt);
		return itemStack;
	}
}