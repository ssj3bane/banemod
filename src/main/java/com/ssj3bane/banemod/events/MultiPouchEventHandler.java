package com.ssj3bane.banemod.events;

import com.ssj3bane.banemod.items.ItemMultiPouch;
import com.ssj3bane.banemod.items.ItemMultiPouchLarge;
import com.ssj3bane.banemod.network.PacketHandler;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.common.util.Constants;

public class MultiPouchEventHandler {
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onMouseEvent(MouseEvent event) {
		EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
		if(event.dwheel != 0 && player != null && player.isSneaking()) {
			ItemStack stack = player.getCurrentEquippedItem();
			if (stack != null) {
				Item item = stack.getItem();
				if (item instanceof ItemMultiPouch) {
					changeSelectedSlot(stack, player, event.dwheel);
					event.setCanceled(true);
				} else if (item instanceof ItemMultiPouchLarge) {
					changeSelectedSlotLarge(stack, player, event.dwheel);
					event.setCanceled(true);
				}
			}
		}
	}

	private void changeSelectedSlot(ItemStack stack, EntityPlayer player, int dWheel) {
		ItemMultiPouch.changeSelectedSlot(stack, dWheel);
		NBTTagCompound nbt = ItemMultiPouch.setupNewNBT(stack);
    	int inventoryLength = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).tagCount() - 1;
    	int slotSelected = nbt.getInteger("SlotSelected");
		PacketHandler.INSTANCE.sendToServer(new MultiPouchPacketProcessor(player.inventory.currentItem, slotSelected, inventoryLength));
	}
	private void changeSelectedSlotLarge(ItemStack stack, EntityPlayer player, int dWheel) {
		ItemMultiPouchLarge.changeSelectedSlot(stack, dWheel);
		NBTTagCompound nbt = ItemMultiPouchLarge.setupNewNBT(stack);
    	int inventoryLength = nbt.getTagList("ItemInventory", Constants.NBT.TAG_COMPOUND).tagCount() - 1;
    	int slotSelected = nbt.getInteger("SlotSelected");
		PacketHandler.INSTANCE.sendToServer(new MultiPouchPacketProcessor(player.inventory.currentItem, slotSelected, inventoryLength));
	}
}