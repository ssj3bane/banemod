package com.ssj3bane.banemod.events;

import com.ssj3bane.banemod.items.ItemMultiPouch;
import com.ssj3bane.banemod.items.ItemMultiPouchLarge;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;

public class MultiPouchPacketProcessor implements IMessage, IMessageHandler<MultiPouchPacketProcessor, IMessage> {

	  private int slot;
	  private int slotSelected;
	  private int pouchMaxSize;
	  
	  public MultiPouchPacketProcessor() {
	  }

	  public MultiPouchPacketProcessor(int slot, int slotSelected, int pouchMaxSize) {
	    this.slot = slot;
	    this.slotSelected = slotSelected;
	    this.pouchMaxSize = pouchMaxSize;
	  }

	  @Override
	  public void toBytes(ByteBuf buffer) {
	    buffer.writeInt(slot);
	    buffer.writeInt(slotSelected);
	    buffer.writeInt(pouchMaxSize);
	  }

	  @Override
	  public void fromBytes(ByteBuf buffer) {
	    slot = buffer.readInt();
	    slotSelected = buffer.readInt();
	    pouchMaxSize = buffer.readInt();
	  }

	  @Override
	  public IMessage onMessage(MultiPouchPacketProcessor message, MessageContext ctx) {
	    ItemStack stack = null;
	    if(message.slot > -1 && message.slot < 9) {
	      stack = ctx.getServerHandler().playerEntity.inventory.getStackInSlot(message.slot);
	    }
	    if(stack != null) {
	    	if(stack.getItem() instanceof ItemMultiPouch) {
	    		ItemMultiPouch.setSlotSelected(stack, message.slotSelected);
	    	} else if(stack.getItem() instanceof ItemMultiPouchLarge) {
	    		ItemMultiPouchLarge.setSlotSelected(stack, message.slotSelected);
	    	}
	    }
	    return null;
	  }
	}