package com.ssj3bane.banemod.client;

import com.ssj3bane.banemod.BaneMod;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class BaneTab extends CreativeTabs {
	public BaneTab() {
		super(BaneMod.MODID);
	}
	
	@Override
	public Item getTabIconItem()
    {
    	return BaneMod.itemBane;
    }
}