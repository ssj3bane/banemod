package com.ssj3bane.banemod;

import java.io.File;

import com.ssj3bane.banemod.blocks.BlockAndesite;
import com.ssj3bane.banemod.blocks.BlockAndesiteSmooth;
import com.ssj3bane.banemod.blocks.BlockConcrete;
import com.ssj3bane.banemod.blocks.BlockDiorite;
import com.ssj3bane.banemod.blocks.BlockDioriteSmooth;
import com.ssj3bane.banemod.blocks.BlockFancyStone;
import com.ssj3bane.banemod.blocks.BlockFlatColor;
import com.ssj3bane.banemod.blocks.BlockFlatGlowingColor;
import com.ssj3bane.banemod.blocks.BlockGranite;
import com.ssj3bane.banemod.blocks.BlockGraniteSmooth;
import com.ssj3bane.banemod.blocks.BlockGreenPortal;
import com.ssj3bane.banemod.blocks.BlockNewWater;
import com.ssj3bane.banemod.config.BaneConfig;
import com.ssj3bane.banemod.events.MultiPouchEventHandler;
import com.ssj3bane.banemod.events.MultiPouchPacketProcessor;
import com.ssj3bane.banemod.gui.GuiHandler;
import com.ssj3bane.banemod.items.ItemBackstabDagger;
import com.ssj3bane.banemod.items.ItemBackstabDaggerHilt;
import com.ssj3bane.banemod.items.ItemBane;
import com.ssj3bane.banemod.items.ItemMedicalPlant;
import com.ssj3bane.banemod.items.ItemMedicalPlantProduct;
import com.ssj3bane.banemod.items.ItemMultiPouch;
import com.ssj3bane.banemod.items.ItemMultiPouchLarge;
import com.ssj3bane.banemod.items.ItemNeedle;
import com.ssj3bane.banemod.network.PacketHandler;
import com.ssj3bane.banemod.proxy.CommonProxy;
import com.ssj3bane.banemod.tileentity.TileEntityGreenPortal;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid = BaneMod.MODID, version = BaneMod.VERSION)
public class BaneMod
{
    public static final String MODID = "banemod";
    public static final String VERSION = "1.0";
    @Mod.Instance(MODID)
    public static BaneMod instance;
    public enum GUI_ENUM
    {
    	CHEFTABLE, MULTIPOUCH, MULTIPOUCHLARGE
    }
	public static int mpWidth = 5;
	public static int mpHeight = 3;
	public static String[] bannedItems;
    private static File configDir;
    @SidedProxy(clientSide = "com.ssj3bane.banemod.proxy.ClientProxy", serverSide = "com.ssj3bane.banemod.proxy.CommonProxy")
    public static CommonProxy proxy;
//    public static CreativeTabs tabName = new BaneTab();
    public static ItemMultiPouch itemMultiPouch = ItemMultiPouch.create();
    public static ItemMultiPouchLarge itemMultiPouchLarge = ItemMultiPouchLarge.create();
    public static ItemBane itemBane = ItemBane.create();
    public static ItemBackstabDaggerHilt itemBackstabDaggerHilt = ItemBackstabDaggerHilt.create();
    public static ItemBackstabDagger itemBackstabDaggerWood = ItemBackstabDagger.create("itemBackstabDaggerWood", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Blocks.planks,
    }, 3, 256, 2, new ItemStack[] {
    	new ItemStack(Blocks.planks)
    });
    public static ItemBackstabDagger itemBackstabDaggerStone = ItemBackstabDagger.create("itemBackstabDaggerStone", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Blocks.cobblestone,
    }, 4, 512, 2, new ItemStack[] {
    	new ItemStack(Blocks.cobblestone)
    });
    public static ItemBackstabDagger itemBackstabDaggerCoal = ItemBackstabDagger.create("itemBackstabDaggerCoal", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Blocks.coal_block,
    }, 5, 512, 2, new ItemStack[] {
    	new ItemStack(Blocks.coal_block)
    });
    public static ItemBackstabDagger itemBackstabDaggerRedstone = ItemBackstabDagger.create("itemBackstabDaggerRedstone", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Blocks.redstone_block,
    }, 6, 512, 2, new ItemStack[] {
    	new ItemStack(Blocks.redstone_block)
    });
    public static ItemBackstabDagger itemBackstabDaggerLapis = ItemBackstabDagger.create("itemBackstabDaggerLapis", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Blocks.lapis_block,
    }, 7, 512, 2, new ItemStack[] {
    	new ItemStack(Blocks.lapis_block)
    });
    public static ItemBackstabDagger itemBackstabDaggerIron = ItemBackstabDagger.create("itemBackstabDaggerIron", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Items.iron_ingot,
    }, 8, 1024, 2, new ItemStack[] {
    	new ItemStack(Items.iron_ingot)
    });
    public static ItemBackstabDagger itemBackstabDaggerGold = ItemBackstabDagger.create("itemBackstabDaggerGold", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Items.gold_ingot,
    }, 9, 1024, 3.5, new ItemStack[] {
    	new ItemStack(Items.gold_ingot)
    });
    public static ItemBackstabDagger itemBackstabDaggerEmerald = ItemBackstabDagger.create("itemBackstabDaggerEmerald", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Items.emerald,
    }, 10, 1024, 2, new ItemStack[] {
    	new ItemStack(Items.emerald)
    });
    public static ItemBackstabDagger itemBackstabDaggerDiamond = ItemBackstabDagger.create("itemBackstabDaggerDiamond", new Object[] {
    	"  m",
    	" m ",
    	"h  ",
    	'h', itemBackstabDaggerHilt,
    	'm', Items.diamond,
    }, 11, 2048, 3, new ItemStack[] {
    	new ItemStack(Items.diamond)
    });
    public static ItemBackstabDagger itemBackstabDaggerDagger = ItemBackstabDagger.createShapeless("itemBackstabDaggerDagger", new Object[] {
    	itemBackstabDaggerWood,
    	itemBackstabDaggerStone,
    	itemBackstabDaggerCoal,
    	itemBackstabDaggerRedstone,
    	itemBackstabDaggerLapis,
    	itemBackstabDaggerIron,
    	itemBackstabDaggerGold,
    	itemBackstabDaggerEmerald,
    	itemBackstabDaggerDiamond
    }, 40, 4096, 5, new ItemStack[] {
    	new ItemStack(itemBackstabDaggerDiamond)
    });
    public static ItemMedicalPlant itemMedicalPlant = ItemMedicalPlant.create();
    public static ItemMedicalPlantProduct itemMedicalPlantProduct = ItemMedicalPlantProduct.create();
    public static ItemNeedle itemNeedle = ItemNeedle.create();
    
    
	public static BlockDiorite blockDiorite = new BlockDiorite();
	public static BlockDioriteSmooth blockDioriteSmooth = new BlockDioriteSmooth();
	public static BlockAndesite blockAndesite = new BlockAndesite();
	public static BlockAndesiteSmooth blockAndesiteSmooth = new BlockAndesiteSmooth();
	public static BlockGranite blockGranite = new BlockGranite();
	public static BlockGraniteSmooth blockGraniteSmooth = new BlockGraniteSmooth();
//	public static BlockDioriteWall blockDioriteWall = new BlockDioriteWall(blockDiorite);
//	public static BlockDioriteSmoothWall blockDioriteSmoothWall = new BlockDioriteSmoothWall(blockDioriteSmooth);
//	public static BlockAndesiteWall blockAndesiteWall = new BlockAndesiteWall(blockAndesite);
//	public static BlockAndesiteSmoothWall blockAndesiteSmoothWall = new BlockAndesiteSmoothWall(blockAndesiteSmooth);
//	public static BlockGraniteWall blockGraniteWall = new BlockGraniteWall(blockGranite);
//	public static BlockGraniteSmoothWall blockGraniteSmoothWall = new BlockGraniteSmoothWall(blockGraniteSmooth);
	public static BlockFancyStone blockFancyStone = new BlockFancyStone();
	public static BlockDarkPrismarine blockDarkPrismarine = new BlockDarkPrismarine();
	public static BlockConcrete blockConcreteWhite = new BlockConcrete("white");
	public static BlockConcrete blockConcreteOrange = new BlockConcrete("orange");
	public static BlockConcrete blockConcreteMagenta = new BlockConcrete("magenta");
	public static BlockConcrete blockConcreteLightBlue = new BlockConcrete("lightBlue");
	public static BlockConcrete blockConcreteYellow = new BlockConcrete("yellow");
	public static BlockConcrete blockConcreteLime = new BlockConcrete("lime");
	public static BlockConcrete blockConcretePink = new BlockConcrete("pink");
	public static BlockConcrete blockConcreteGray = new BlockConcrete("gray");
	public static BlockConcrete blockConcreteLightGray = new BlockConcrete("lightGray");
	public static BlockConcrete blockConcreteCyan = new BlockConcrete("cyan");
	public static BlockConcrete blockConcretePurple = new BlockConcrete("purple");
	public static BlockConcrete blockConcreteBlue = new BlockConcrete("blue");
	public static BlockConcrete blockConcreteBrown = new BlockConcrete("brown");
	public static BlockConcrete blockConcreteGreen = new BlockConcrete("green");
	public static BlockConcrete blockConcreteRed = new BlockConcrete("red");
	public static BlockConcrete blockConcreteBlack = new BlockConcrete("black");
	public static BlockFlatColor blockFlatColorDawn = new BlockFlatColor("dawn");
	public static BlockFlatColor blockFlatColorDusk = new BlockFlatColor("dusk");
	public static BlockFlatColor blockFlatColorMidday = new BlockFlatColor("midday");
	public static BlockFlatColor blockFlatColorNight = new BlockFlatColor("night");
	public static BlockFlatColor blockFlatColorMidnight = new BlockFlatColor("midnight");
	public static BlockFlatGlowingColor blockFlatGlowingColorDawn = new BlockFlatGlowingColor("dawn");
	public static BlockFlatGlowingColor blockFlatGlowingColorDusk = new BlockFlatGlowingColor("dusk");
	public static BlockFlatGlowingColor blockFlatGlowingColorMidday = new BlockFlatGlowingColor("midday");
	public static BlockFlatGlowingColor blockFlatGlowingColorNight = new BlockFlatGlowingColor("night");
	public static BlockFlatGlowingColor blockFlatGlowingColorMidnight = new BlockFlatGlowingColor("midnight");
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	configDir = new File(event.getModConfigurationDirectory() + "/" + MODID);
		configDir.mkdirs();
    	BaneConfig.init(configDir);
    	MultiPouchEventHandler mpeh = new MultiPouchEventHandler();
    	MinecraftForge.EVENT_BUS.register(mpeh);
    	FMLCommonHandler.instance().bus().register(mpeh);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	PacketHandler.INSTANCE.registerMessage(MultiPouchPacketProcessor.class, MultiPouchPacketProcessor.class, PacketHandler.nextID(), Side.SERVER);
    	NetworkRegistry.INSTANCE.registerGuiHandler(BaneMod.instance, new GuiHandler());
    	GameRegistry.registerTileEntity(TileEntityGreenPortal.class, "GreenPortal");
    	/*
    	 * Blocks
    	 */
    	GameRegistry.registerBlock(new BlockGreenPortal(), "blockGreenPortal");
    	GameRegistry.registerBlock(blockDiorite, "blockDiorite");
    	GameRegistry.registerBlock(blockDioriteSmooth, "blockDioriteSmooth");
    	GameRegistry.registerBlock(blockAndesite, "blockAndesite");
    	GameRegistry.registerBlock(blockAndesiteSmooth, "blockAndesiteSmooth");
    	GameRegistry.registerBlock(blockGranite, "blockGranite");
    	GameRegistry.registerBlock(blockGraniteSmooth, "blockGraniteSmooth");
//    	GameRegistry.registerBlock(blockDioriteWall, "blockDioriteWall");
//    	GameRegistry.registerBlock(blockDioriteSmoothWall, "blockDioriteSmoothWall");
//    	GameRegistry.registerBlock(blockAndesiteWall, "blockAndesiteWall");
//    	GameRegistry.registerBlock(blockAndesiteSmoothWall, "blockAndesiteSmoothWall");
//    	GameRegistry.registerBlock(blockGraniteWall, "blockGraniteWall");
//    	GameRegistry.registerBlock(blockGraniteSmoothWall, "blockGraniteSmoothWall");
    	GameRegistry.registerBlock(blockFancyStone, "blockFancyStone");
    	GameRegistry.registerBlock(blockConcreteWhite, blockConcreteWhite.fullName);
    	GameRegistry.registerBlock(blockConcreteOrange, blockConcreteOrange.fullName);
    	GameRegistry.registerBlock(blockConcreteMagenta, blockConcreteMagenta.fullName);
    	GameRegistry.registerBlock(blockConcreteLightBlue, blockConcreteLightBlue.fullName);
    	GameRegistry.registerBlock(blockConcreteYellow, blockConcreteYellow.fullName);
    	GameRegistry.registerBlock(blockConcreteLime, blockConcreteLime.fullName);
    	GameRegistry.registerBlock(blockConcretePink, blockConcretePink.fullName);
    	GameRegistry.registerBlock(blockConcreteGray, blockConcreteGray.fullName);
    	GameRegistry.registerBlock(blockConcreteLightGray, blockConcreteLightGray.fullName);
    	GameRegistry.registerBlock(blockConcreteCyan, blockConcreteCyan.fullName);
    	GameRegistry.registerBlock(blockConcretePurple, blockConcretePurple.fullName);
    	GameRegistry.registerBlock(blockConcreteBlue, blockConcreteBlue.fullName);
    	GameRegistry.registerBlock(blockConcreteBrown, blockConcreteBrown.fullName);
    	GameRegistry.registerBlock(blockConcreteGreen, blockConcreteGreen.fullName);
    	GameRegistry.registerBlock(blockConcreteRed, blockConcreteRed.fullName);
    	GameRegistry.registerBlock(blockConcreteBlack, blockConcreteBlack.fullName);
    	
    	GameRegistry.registerBlock(blockFlatColorDawn, blockFlatColorDawn.fullName);
    	GameRegistry.registerBlock(blockFlatColorDusk, blockFlatColorDusk.fullName);
    	GameRegistry.registerBlock(blockFlatColorMidday, blockFlatColorMidday.fullName);
    	GameRegistry.registerBlock(blockFlatColorNight, blockFlatColorNight.fullName);
    	GameRegistry.registerBlock(blockFlatColorMidnight, blockFlatColorMidnight.fullName);
    	GameRegistry.registerBlock(blockFlatGlowingColorDawn, blockFlatGlowingColorDawn.fullName);
    	GameRegistry.registerBlock(blockFlatGlowingColorDusk, blockFlatGlowingColorDusk.fullName);
    	GameRegistry.registerBlock(blockFlatGlowingColorMidday, blockFlatGlowingColorMidday.fullName);
    	GameRegistry.registerBlock(blockFlatGlowingColorNight, blockFlatGlowingColorNight.fullName);
    	GameRegistry.registerBlock(blockFlatGlowingColorMidnight, blockFlatGlowingColorMidnight.fullName);
    	
    	GameRegistry.registerBlock(blockDarkPrismarine, "blockDarkPrismarine");
    	/*
    	 * Water
    	 */
    	GameRegistry.registerBlock(new BlockNewWater("blockWhiteWater",		true, 1), "blockWhiteWater");
    	GameRegistry.registerBlock(new BlockNewWater("blockBlackWater",		false, 0), "blockBlackWater");
    	GameRegistry.registerBlock(new BlockNewWater("blockPinkWater",		false, 0), "blockPinkWater");
    	GameRegistry.registerBlock(new BlockNewWater("blockRedWater",		false, 0), "blockRedWater");
    	GameRegistry.registerBlock(new BlockNewWater("blockOrangeWater",	false, 0), "blockOrangeWater");
    	GameRegistry.registerBlock(new BlockNewWater("blockPurpleWater",	false, 0), "blockPurpleWater");
    	GameRegistry.registerBlock(new BlockNewWater("blockYellowWater",	false, 0), "blockYellowWater");
    	/*
    	 * Recipes
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(itemMultiPouch), ItemMultiPouch.recipe);
    	GameRegistry.addShapedRecipe(new ItemStack(itemMultiPouchLarge), ItemMultiPouchLarge.recipe);
    	/*
    	 * Cobblestone to Diorite
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockDiorite, 4), BlockDiorite.recipe);
    	/*
    	 * Diorite to Andesite
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockAndesite, 1), BlockAndesite.recipe);
    	/*
    	 * Andesite to Granite
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockGranite, 1), BlockGranite.recipe);
    	/*
    	 * Granite to Diorite
    	 */
//    	GameRegistry.addShapedRecipe(new ItemStack(blockDiorite, 1), BlockGranite.recipeLoop);
    	/*
    	 * Slabs to Fancy Stone
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockFancyStone, 1), BlockFancyStone.recipe);
    	/*
    	 * Diorite to Smooth Diorite
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockDioriteSmooth, 4), BlockDioriteSmooth.recipe);
    	/*
    	 * Andesite to Smooth Andesite
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockAndesiteSmooth, 4), BlockAndesiteSmooth.recipe);
    	/*
    	 * Granite to Smooth Granite
    	 */
    	GameRegistry.addShapedRecipe(new ItemStack(blockGraniteSmooth, 4), BlockGraniteSmooth.recipe);
    }
}
