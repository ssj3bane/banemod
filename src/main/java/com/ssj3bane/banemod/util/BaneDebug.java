package com.ssj3bane.banemod.util;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BaneDebug{
	public BaneDebug() {
		
	}
	@SideOnly(Side.CLIENT)
	public static void cprintln(String arg) {
		System.out.print("Client: ");
		System.out.println(arg);
	}
	@SideOnly(Side.SERVER)
	public static void sprintln(String arg) {
		System.out.print("Server: ");
		System.out.println(arg);
	}
}