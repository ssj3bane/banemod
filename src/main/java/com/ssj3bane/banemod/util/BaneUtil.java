package com.ssj3bane.banemod.util;

import com.ssj3bane.banemod.BaneMod;

import net.minecraft.item.ItemStack;

public class BaneUtil {
	public static boolean isItemBanned(String itemName) {
		for(String bannedItem : BaneMod.bannedItems) {
			if(bannedItem.equals(itemName)) {
				return true;
			}
		}
		return false;
	}
	public static boolean contains(ItemStack[] array, ItemStack itemStack) {
		for(ItemStack check : array)
			if(itemStack.equals(check))
				return true;
		return false;
	}
}