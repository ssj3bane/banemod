package com.ssj3bane.banemod;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

public class BlockDarkPrismarine extends Block {
	private IIcon icon;
	public BlockDarkPrismarine() {
		super(Material.rock);
		setHardness(20);
		setBlockName("blockDarkPrismarine");
		setBlockTextureName(BaneMod.MODID + ":BlockDarkPrismarine");
		setCreativeTab(CreativeTabs.tabBlock);
	}
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
        return this.icon; // != 0 && p_149691_1_ != 1 ? this.field_149806_a[1] : this.field_149806_a[0];
    }
	@SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
    	this.icon = iconRegister.registerIcon(this.getTextureName());
    }
}
