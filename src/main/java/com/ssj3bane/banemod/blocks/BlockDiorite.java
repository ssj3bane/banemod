package com.ssj3bane.banemod.blocks;


import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.List;
import java.util.Random;

import com.ssj3bane.banemod.BaneMod;

public class BlockDiorite extends Block {
	public static Object[] recipe = new Object[] {
		"CC",
		"CC",
		'C', Blocks.cobblestone,
	};
	public BlockDiorite() {
		super(Material.rock);
		setHardness(69);
		setResistance(69 / 3.0F);
		setCreativeTab(CreativeTabs.tabBlock);
		setBlockName("blockDiorite");
		setBlockTextureName(BaneMod.MODID + ":" + "BlockDiorite");
        setStepSound(soundTypeStone);
		// TODO Auto-generated constructor stub
	}
	
	public static Block blockDiorite;
	
	@SideOnly(Side.CLIENT)
	private IIcon icon;
	
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata) {
        return this.icon;
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
    	this.icon = iconRegister.registerIcon(this.getTextureName());
    }
    
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
        return Item.getItemFromBlock(blockDiorite);
    }
    
    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
    }
 
}