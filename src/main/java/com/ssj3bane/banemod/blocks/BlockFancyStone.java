package com.ssj3bane.banemod.blocks;

import java.util.List;
import java.util.Random;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockFancyStone extends Block {
	public static Object[] recipe = new Object[] {
		"S",
		"S",
		'S', Blocks.stone_slab,
	};
	public BlockFancyStone() {
		super(Material.rock);
		setCreativeTab(CreativeTabs.tabBlock);
		setBlockName("blockFancyStone");
		setBlockTextureName(BaneMod.MODID + ":" + "BlockFancyStone");
		// TODO Auto-generated constructor stub
	}
	
	public static Block blockFancyStone;
	
	@SideOnly(Side.CLIENT)
	private IIcon icon;
	
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata) {
        return this.icon;
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
    	this.icon = iconRegister.registerIcon(this.getTextureName());
    }
    
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Item.getItemFromBlock(blockFancyStone);
    }
    
    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
    }
}
