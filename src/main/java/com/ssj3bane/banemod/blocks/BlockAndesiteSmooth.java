package com.ssj3bane.banemod.blocks;

import java.util.List;
import java.util.Random;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockAndesiteSmooth extends Block {
	public static Object[] recipe = new Object[] {
		"AA",
		"AA",
		'A', BaneMod.blockAndesite
	};
	public BlockAndesiteSmooth() {
		super(Material.rock);
		setHardness(69);
		setResistance(69 / 3.0F);
		setCreativeTab(CreativeTabs.tabBlock);
		setBlockName("blockAndesiteSmooth");
		setBlockTextureName(BaneMod.MODID + ":" + "BlockAndesiteSmooth");
        setStepSound(soundTypeStone);
		// TODO Auto-generated constructor stub
	}
	
	public static Block blockAndesite;
	
	@SideOnly(Side.CLIENT)
	private IIcon icon;
	
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata) {
        return this.icon;
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
    	this.icon = iconRegister.registerIcon(this.getTextureName());
    }
    
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Item.getItemFromBlock(blockAndesite);
    }
    
    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
    }
}
