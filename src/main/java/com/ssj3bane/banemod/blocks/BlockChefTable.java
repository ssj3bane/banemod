package com.ssj3bane.banemod.blocks;


import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;

import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import java.util.Random;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.common.registry.GameRegistry;

public class BlockChefTable extends Block
{
	public BlockChefTable() {
		super(Material.wood);
//		setCreativeTab(BaneMod.tabName);
		setBlockName("blockChefTable");
		setBlockTextureName(BaneMod.MODID + ":" + "BlockChefTable");
		// TODO Auto-generated constructor stub
	}
	
	public static Block blockChefTable;
	
	@SideOnly(Side.CLIENT)
	private IIcon icon0;
	@SideOnly(Side.CLIENT)
	private IIcon icon1;
	@SideOnly(Side.CLIENT)
	private IIcon icon2;
	@SideOnly(Side.CLIENT)
	private IIcon icon3;
	@SideOnly(Side.CLIENT)
	private IIcon icon4;
	@SideOnly(Side.CLIENT)
	private IIcon icon5;
	
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata)
    {
		return side == 0 ? this.icon0 : (side == 1 ? this.icon1 : (side == 2 ? this.icon2 : (side == 3 ? this.icon3 : (side == 4 ? this.icon4 : this.icon5))));
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister)
    {
        this.icon0 = iconRegister.registerIcon(this.getTextureName() + "_0");
        this.icon1 = iconRegister.registerIcon(this.getTextureName() + "_1");
        this.icon2 = iconRegister.registerIcon(this.getTextureName() + "_2");
        this.icon3 = iconRegister.registerIcon(this.getTextureName() + "_3");
        this.icon4 = iconRegister.registerIcon(this.getTextureName() + "_4");
        this.icon5 = iconRegister.registerIcon(this.getTextureName() + "_5");
    }
    
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Item.getItemFromBlock(blockChefTable);
    }
    
    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World world, int x, int y, int z)
    {
        super.onBlockAdded(world, x, y, z);
    }
 
}