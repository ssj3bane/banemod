package com.ssj3bane.banemod.blocks;

import com.ssj3bane.banemod.BaneMod;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

public class BlockFlatGlowingColor extends Block {
	private IIcon icon;
	public String fullName = "";
	public BlockFlatGlowingColor(String color) {
		super(Material.iron);
		String name = "blockFlatColor";
		String properColor = color.substring(0, 1).toUpperCase() + color.substring(1);
		this.fullName = name + "Glowing" + properColor;
		String properName = name.substring(0, 1).toUpperCase() + name.substring(1);
		setHardness(20);
		setLightLevel(1.0F);
		setBlockName(name + "Glowing" + properColor);
		setBlockTextureName(BaneMod.MODID + ":" + properName + properColor);
		setCreativeTab(CreativeTabs.tabBlock);
	}
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
        return this.icon; // != 0 && p_149691_1_ != 1 ? this.field_149806_a[1] : this.field_149806_a[0];
    }
	@SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
    	this.icon = iconRegister.registerIcon(this.getTextureName());
    }
}
