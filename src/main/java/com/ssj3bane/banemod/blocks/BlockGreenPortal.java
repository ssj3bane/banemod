package com.ssj3bane.banemod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import java.util.Random;

import com.ssj3bane.banemod.BaneMod;
import com.ssj3bane.banemod.tileentity.TileEntityGreenPortal;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockGreenPortal extends BlockContainer
{
    public static boolean field_149948_a;
    public BlockGreenPortal() {
        super(Material.water);
        this.setLightLevel(1.0F);
        this.setTickRandomly(true);
		setBlockTextureName(BaneMod.MODID + ":" + "BlockGreenPortal");
    }

    /**
     * Returns a new instance of a block's tile entity class. Called on placing the block.
     */
    public TileEntity createNewTileEntity(World world, int instanceNumber) {
        return new TileEntityGreenPortal();
    }

    /**
     * Updates the blocks bounds based on its current state. Args: world, x, y, z
     */
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
        float f = 0.375F;
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, f, 1.0F);
    }

    /**
     * Returns true if the given side of this block type should be rendered, if the adjacent block is at the given
     * coordinates.  Args: blockAccess, x, y, z, side
     */
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess blockAccess, int x, int y, int z, int side)
    {
        Block block = blockAccess.getBlock(x, y, z);
        
        return block == this ? false : super.shouldSideBeRendered(blockAccess, x, y, z, side);
    }

    /**
     * Adds all intersecting collision boxes to a list. (Be sure to only add boxes to the list if they intersect the
     * mask.) Parameters: World, X, Y, Z, mask, list, colliding entity
     */
    public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB mask, @SuppressWarnings("rawtypes") List list, Entity collidingEntity) {
    	
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean isOpaqueCube() {
        return false;
    }

    /**
     * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
     */
    public boolean renderAsNormalBlock() {
        return false;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random p_149745_1_) {
        return 0;
    }

    /**
     * Triggered whenever an entity collides with this block (enters into the block). Args: world, x, y, z, entity
     */
    public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
    	/*
        if (entity.ridingEntity == null && entity.riddenByEntity == null && !world.isRemote) {
            entity.travelToDimension(1);
        }
        */
    }
    @SideOnly(Side.CLIENT)
    public void updateTick(World world, int x, int y, int z, Random random) {
		randomDisplayTick(world, x, y, z, random);
    }
    
    /**
     * A randomly called display update to be able to add particles or other items for display
     */
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int x, int y, int z, Random random) {
    	if(random.nextBoolean() == true) {
	        double d0 = (double)((float)x + random.nextFloat());
	        double d1 = (double)((float)y + random.nextFloat());
	        double d2 = (double)((float)z + random.nextFloat());
	        double d3 = 0.0D;
	        double d4 = 0.4D;
	        double d5 = 0.0D;
	        world.spawnParticle("happyVillager", d0, d1, d2, d3, d4, d5);
    	}
    }


    @SideOnly(Side.CLIENT)
    public int getRenderBlockPass()
    {
        return 1;
    }
    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World world, int x, int y, int z) {
        /*
    	if (!field_149948_a) {
            if (world.provider.dimensionId != 0) {
                world.setBlockToAir(x, y, z);
            }
        }
        */
    }

    /**
     * Gets an item for the block being called on. Args: world, x, y, z
     */
    @SideOnly(Side.CLIENT)
    public Item getItem(World p_149694_1_, int p_149694_2_, int p_149694_3_, int p_149694_4_) {
        return Item.getItemById(0);
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegistry) {
//        this.blockIcon = iconRegistry.registerIcon("banemod:green_portal");
        this.blockIcon = iconRegistry.registerIcon(this.getTextureName());
    }

	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata) {
        return this.blockIcon;
    }
	
    public MapColor getMapColor(int p_149728_1_) {
        return MapColor.emeraldColor;
    }
}