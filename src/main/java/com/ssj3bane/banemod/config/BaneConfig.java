package com.ssj3bane.banemod.config;

import java.io.File;
import java.util.List;

import com.google.common.collect.Lists;
import com.ssj3bane.banemod.BaneMod;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class BaneConfig {
	public static Configuration main;
	public static int mpWidth = 5;
	public static int mpHeight = 3;
	public static String[] bannedItems;
	public static void init(File dir)
	{
		main = new Configuration(new File(dir.getPath(), "baneconfig.cfg"));
		
		sync();
	}
	
	private static void sync()
	{
		syncMain();
	}
	
	private static void syncMain()
	{
		List<String> propOrder = Lists.newArrayList();
		String category = Configuration.CATEGORY_GENERAL;
		main.load();
		Property multiPouchSlotsWidth = main.get(category, "Width", "5", "Amount of slots width-wise the MultiPouch should have");
		propOrder.add(multiPouchSlotsWidth.getName());
		Property multiPouchSlotsHeight = main.get(category, "Height", "3", "Amount of slots height-wise the MultiPouch should have");
		propOrder.add(multiPouchSlotsHeight.getName());
		Property multiPouchBannedItems = main.get(category, "Banned Items", new String[] {"minecraft:water_bucket", "minecraft:lava_bucket"}, "Items that are unable to be used by the MultiPouch");
		propOrder.add(multiPouchBannedItems.getName());
		mpWidth = multiPouchSlotsWidth.getInt();
		mpHeight = multiPouchSlotsHeight.getInt();
		bannedItems = multiPouchBannedItems.getStringList();
		BaneMod.mpWidth = mpWidth;
		BaneMod.mpHeight = mpHeight;
		BaneMod.bannedItems = bannedItems;
		main.setCategoryPropertyOrder(category, propOrder);
		main.save();
	}
}