package com.ssj3bane.banemod.gui;

import com.ssj3bane.banemod.BaneMod;
import com.ssj3bane.banemod.container.ContainerMultiPouch;
import com.ssj3bane.banemod.container.ContainerMultiPouchLarge;
import com.ssj3bane.banemod.inventory.ItemInventory;
import com.ssj3bane.banemod.inventory.ItemInventoryLarge;

import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler
{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) { 
		if (ID == BaneMod.GUI_ENUM.MULTIPOUCH.ordinal())
		{
			return new ContainerMultiPouch(player, player.inventory, new ItemInventory(player.getHeldItem()));
		}
		if (ID == BaneMod.GUI_ENUM.MULTIPOUCHLARGE.ordinal())
		{
			return new ContainerMultiPouchLarge(player, player.inventory, new ItemInventoryLarge(player.getHeldItem()));
		}

		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID == BaneMod.GUI_ENUM.MULTIPOUCH.ordinal()) {
			return new GuiMultiPouch((ContainerMultiPouch) new ContainerMultiPouch(player, player.inventory, new ItemInventory(player.getHeldItem())));
		}
		if (ID == BaneMod.GUI_ENUM.MULTIPOUCHLARGE.ordinal()) {
			return new GuiMultiPouchLarge((ContainerMultiPouchLarge) new ContainerMultiPouchLarge(player, player.inventory, new ItemInventoryLarge(player.getHeldItem())));
		}
		return null;
	}
}