package com.inforno.backstab.items;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.util.ResourceLocation;



public class Items
{
  public static final List<Item> items = new ArrayList();
  public static Item wooddagger;
  public static Item stonedagger;
  public static Item irondagger;
  public static Item golddagger;
  public static Item diamonddagger;
  
  public Items() {}
  
  public static final void preinit() { wooddagger = registerItem(new ItemDagger(Item.ToolMaterial.WOOD), "wood_dagger");
    stonedagger = registerItem(new ItemDagger(Item.ToolMaterial.STONE), "stone_dagger");
    irondagger = registerItem(new ItemDagger(Item.ToolMaterial.IRON), "iron_dagger");
    golddagger = registerItem(new ItemDagger(Item.ToolMaterial.GOLD), "gold_dagger");
    diamonddagger = registerItem(new ItemDagger(Item.ToolMaterial.DIAMOND), "diamond_dagger");
  }
  
  public static final void registerRenders() {
    for (Item i : items) {
      Minecraft.func_71410_x().func_175599_af().func_175037_a().func_178086_a(i, 0, new ModelResourceLocation("backstab:" + i
        .getRegistryName().func_110623_a(), "inventory"));
    }
  }
  
  public static final Item registerItem(Item i, String n) {
    ResourceLocation r = new ResourceLocation("backstab", n);
    i.func_77655_b(n);
    i.setRegistryName(r);
    items.add(i);
    return i;
  }
}
