package com.inforno.backstab.events;

import com.inforno.backstab.items.Items;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistryEntry;

@net.minecraftforge.fml.common.Mod.EventBusSubscriber
public class Registries
{
  public Registries() {}
  
  @net.minecraftforge.fml.common.eventhandler.SubscribeEvent
  public static void ItemRegister(RegistryEvent.Register<Item> event)
  {
    if (com.inforno.backstab.config.Config.backstabItemsEnabled) {
      event.getRegistry().registerAll((IForgeRegistryEntry[])Items.items.toArray(new Item[0]));
    }
  }
}
