package com.inforno.backstab.events;

import com.inforno.backstab.config.Config;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.registry.RegistryNamespaced;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class Events
{
  public Events() {}
  
  @SubscribeEvent(priority=EventPriority.HIGHEST)
  public void onLivingHurtEvent(LivingHurtEvent event)
  {
    if ((event.getSource().func_76364_f() != null) && ((event.getSource().func_76364_f() instanceof EntityPlayer))) {
      EntityPlayer player = (EntityPlayer)event.getSource().func_76364_f();
      if ((!field_70170_p.field_72995_K) && 
        ((containsNoCase(Config.items, player.func_184614_ca().func_77973_b().getRegistryName().toString())) || (Config.backstabItemsAll == true)) && 
        (!containsNoCase(Config.entities, EntityList.func_191306_a(event.getEntity().getClass()).toString())) && (
        (!Config.backstabSneaking) || ((Config.backstabSneaking) && (player.func_70093_af())))) {
        backstab(event, player);
      }
    }
  }
  
  public void backstab(LivingHurtEvent event, EntityPlayer player)
  {
    double targetYaw = event.getEntityLiving().func_70079_am() % 360.0F;
    double playerYaw = event.getSource().func_76346_g().func_70079_am() % 360.0F;
    if (playerYaw < 0.0D) playerYaw += 360.0D;
    if (targetYaw < 0.0D) targetYaw += 360.0D;
    if ((Math.abs(targetYaw - playerYaw) < Config.backstabDegrees) || (360.0D - Math.abs(targetYaw - playerYaw) < Config.backstabDegrees)) {
      event.setAmount((float)(event.getAmount() * Config.backstabMultiplier));
      field_70170_p.func_184148_a(null, field_70165_t, field_70163_u, field_70161_v, getRegisteredSoundEvent(Config.backstabSound), SoundCategory.PLAYERS, (float)Config.backstabPitch, (float)Config.backstabVolume);
    }
  }
  
  private boolean containsNoCase(List<String> list, String s) {
    for (int i = 0; i < list.toArray().length; i++) {
      if (s.equalsIgnoreCase((String)list.toArray()[i])) {
        return true;
      }
    }
    return false;
  }
  
  private static SoundEvent getRegisteredSoundEvent(String id)
  {
    SoundEvent soundevent = (SoundEvent)SoundEvent.field_187505_a.func_82594_a(new ResourceLocation(id));
    if (soundevent == null) {
      throw new IllegalStateException("Invalid Sound requested: " + id);
    }
    return soundevent;
  }
}
