package com.inforno.backstab.gui;

import com.inforno.backstab.config.Config;
import java.util.List;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class EntitiesConfigGui extends GuiScreen
{
  private GuiButton done;
  private GuiButton add;
  private GuiButton remove;
  private GuiTextField input;
  private GuiScroll list;
  
  public EntitiesConfigGui() {}
  
  public void func_73863_a(int mouseX, int mouseY, float partialTicks)
  {
    func_146276_q_();
    input.func_146194_f();
    func_73731_b(field_146289_q, Config.entitiesString, field_146294_l / 2 - 200, field_146295_m / 2 - 115, 16777215);
    list.drawScreen(mouseX, mouseY, partialTicks);
    super.func_73863_a(mouseX, mouseY, partialTicks);
  }
  
  public void func_73866_w_()
  {
    field_146292_n.clear();
    list = new GuiScroll(this, Config.entities, field_146294_l / 2 + 30, field_146295_m / 2, field_146295_m / 2 - 50, field_146295_m / 2 + 50, field_146294_l / 2 - 200, 20, field_146294_l / 2 - 190);
    input = new GuiTextField(0, field_146289_q, field_146294_l / 2 - 200, field_146295_m / 2 - 100, 200, 20);
    input.func_146203_f(1000);
    field_146292_n.add(this.done = new GuiButton(-1, field_146294_l / 2 - 100, field_146295_m - 33, 200, 20, "Done"));
    field_146292_n.add(this.add = new GuiButton(0, field_146294_l / 2 + 5, field_146295_m / 2 - 100, 60, 20, "Add Entity"));
    field_146292_n.add(this.remove = new GuiButton(1, field_146294_l / 2 + 70, field_146295_m / 2 - 100, 75, 20, "Remove Entity"));
    super.func_73866_w_();
  }
  
  public void func_146284_a(GuiButton button) throws java.io.IOException
  {
    switch (field_146127_k) {
    case -1: 
      field_146297_k.func_147108_a(null);
      break;
    case 0: 
      if (!input.func_146179_b().isEmpty()) {
        Config.entities.add(input.func_146179_b());
        list.updateList(Config.entities);
        func_73866_w_();
      }
      break;
    case 1: 
      Config.entities.remove(list.getIndex());
      list.updateList(Config.entities);
      func_73866_w_();
    }
    
    Config.syncFromFields();
    super.func_146284_a(button);
  }
  
  protected void func_73864_a(int mouseX, int mouseY, int mouseButton) throws java.io.IOException
  {
    input.func_146192_a(mouseX, mouseY, mouseButton);
    super.func_73864_a(mouseX, mouseY, mouseButton);
  }
  
  protected void func_73869_a(char typedChar, int keyCode) throws java.io.IOException
  {
    input.func_146201_a(typedChar, keyCode);
    if (org.lwjgl.input.Keyboard.isKeyDown(28)) {
      input.func_146195_b(false);
    }
    super.func_73869_a(typedChar, keyCode);
  }
}
