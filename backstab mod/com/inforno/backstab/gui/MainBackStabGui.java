package com.inforno.backstab.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class MainBackStabGui extends GuiScreen
{
  private GuiButton done;
  private GuiButton config;
  
  public MainBackStabGui() {}
  
  public void func_73863_a(int mouseX, int mouseY, float partialTicks)
  {
    func_146276_q_();
    super.func_73863_a(mouseX, mouseY, partialTicks);
  }
  
  public void func_73866_w_()
  {
    field_146292_n.clear();
    field_146292_n.add(this.done = new GuiButton(-1, field_146294_l / 2 - 100, field_146295_m - 33, 200, 20, "Done"));
    field_146292_n.add(this.config = new GuiButton(0, field_146294_l / 2 - 100, field_146295_m / 2, 200, 20, "Configurations"));
    super.func_73866_w_();
  }
  
  public void func_146284_a(GuiButton button) throws java.io.IOException
  {
    switch (field_146127_k) {
    case -1: 
      field_146297_k.func_147108_a(null);
      break;
    case 0: 
      field_146297_k.func_147108_a(new ConfigGui());
    }
    
    super.func_146284_a(button);
  }
}
