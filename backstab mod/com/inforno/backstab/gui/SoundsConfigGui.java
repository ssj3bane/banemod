package com.inforno.backstab.gui;

import com.inforno.backstab.config.Config;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraftforge.fml.client.config.GuiSlider;
import org.lwjgl.input.Keyboard;

public class SoundsConfigGui extends GuiScreen
{
  private GuiButton done;
  private GuiTextField sound;
  private GuiSlider pitch;
  private GuiSlider volume;
  private String soundString = "Sound:";
  private DecimalFormat format = new DecimalFormat("#.0");
  
  public SoundsConfigGui() {}
  
  public void func_73863_a(int mouseX, int mouseY, float partialTicks) { func_146276_q_();
    sound.func_146194_f();
    func_73731_b(field_146289_q, soundString, field_146294_l / 2 - 100, field_146295_m / 2 - 25, 16777215);
    drawInformation(Config.soundsString, mouseX, mouseY, field_146294_l / 2 - 100, field_146295_m / 2 - 25, field_146289_q.func_78256_a(soundString), 10);
    super.func_73863_a(mouseX, mouseY, partialTicks);
  }
  
  public void drawInformation(String line, int mouseX, int mouseY, int startX, int startY, int width, int height) {
    if ((mouseX >= startX) && (mouseX <= startX + width) && (mouseY >= startY) && (mouseY <= startY + height)) {
      func_146279_a(line, mouseX, mouseY);
    }
  }
  
  public void func_73866_w_()
  {
    field_146292_n.clear();
    sound = new GuiTextField(0, field_146289_q, field_146294_l / 2 - 43, field_146295_m / 2 - 30, 140, 20);
    sound.func_146203_f(1000);
    sound.func_146180_a(Config.backstabSound);
    field_146292_n.add(this.done = new GuiButton(-1, field_146294_l / 2 - 100, field_146295_m - 33, 200, 20, "Done"));
    field_146292_n.add(this.pitch = new GuiSlider(0, field_146294_l / 2 - 100, field_146295_m / 2, 200, 20, "Pitch: ", "", 0.0D, 5.0D, Config.backstabPitch, true, true));
    field_146292_n.add(this.volume = new GuiSlider(1, field_146294_l / 2 - 100, field_146295_m / 2 + 30, 200, 20, "Volume: ", "", 0.0D, 2.0D, Config.backstabVolume, true, true));
    super.func_73866_w_();
  }
  
  protected void func_73864_a(int mouseX, int mouseY, int mouseButton) throws IOException
  {
    sound.func_146192_a(mouseX, mouseY, mouseButton);
    updateTextBoxes(sound);
    super.func_73864_a(mouseX, mouseY, mouseButton);
  }
  
  protected void func_73869_a(char typedChar, int keyCode) throws IOException
  {
    sound.func_146201_a(typedChar, keyCode);
    updateTextBoxes(sound);
    super.func_73869_a(typedChar, keyCode);
  }
  
  public void func_146284_a(GuiButton button) throws IOException
  {
    switch (field_146127_k) {
    case -1: 
      field_146297_k.func_147108_a(null);
      break;
    case 0: 
      pitch.updateSlider();
      break;
    case 1: 
      volume.updateSlider();
    }
    
    super.func_146284_a(button);
  }
  
  public void func_73876_c()
  {
    pitch.setValue(Double.parseDouble(format.format(pitch.getValue())));
    volume.setValue(Double.parseDouble(format.format(volume.getValue())));
    Config.backstabPitch = Double.parseDouble(format.format(pitch.getValue()));
    Config.backstabVolume = Double.parseDouble(format.format(volume.getValue()));
  }
  
  public void func_146281_b()
  {
    sound.func_146195_b(false);
    updateTextBoxes(sound);
  }
  
  public void updateTextBoxes(GuiTextField textField) {
    if (Keyboard.isKeyDown(28)) {
      textField.func_146195_b(false);
    }
    if ((!textField.func_146179_b().isEmpty()) && (!textField.func_146206_l())) {
      switch (textField.func_175206_d()) {
      case 0: 
        Config.backstabSound = textField.func_146179_b();
      }
      
      Config.syncFromFields();
    }
  }
}
