package com.inforno.backstab.gui;

import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GuiScroll extends net.minecraftforge.fml.client.GuiScrollingList
{
  private GuiScreen parent;
  private int size;
  private int index;
  private int location;
  private List list;
  
  public GuiScroll(GuiScreen parent, List<String> list, int width, int height, int top, int bottom, int left, int entryHeight, int location)
  {
    super(Minecraft.func_71410_x(), width, height, top, bottom, left, entryHeight, field_146294_l, field_146295_m);
    this.parent = parent;
    updateList(list);
    size = list.size();
    this.location = location;
  }
  
  public void updateList(List<String> list) {
    this.list = list;
  }
  
  protected int getSize()
  {
    return size;
  }
  
  protected void elementClicked(int index, boolean doubleClick)
  {
    this.index = index;
  }
  
  protected boolean isSelected(int index)
  {
    return this.index == index;
  }
  
  public int getIndex() {
    return index;
  }
  
  public void setIndex(int index) {
    this.index = index;
  }
  


  protected void drawBackground() {}
  

  protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, net.minecraft.client.renderer.Tessellator tess)
  {
    parent.func_73731_b(func_71410_xfield_71466_p, (String)list.get(slotIdx), location, slotTop + 5, 16777215);
  }
}
