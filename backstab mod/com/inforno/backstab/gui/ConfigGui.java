package com.inforno.backstab.gui;

import com.inforno.backstab.config.Config;
import java.util.List;
import net.minecraft.client.gui.GuiButton;
import net.minecraftforge.fml.client.config.GuiSlider;

public class ConfigGui extends net.minecraft.client.gui.GuiScreen
{
  private GuiButton done;
  private GuiButton sneaking;
  private GuiButton itemsall;
  private GuiButton items;
  private GuiButton entities;
  private GuiButton sounds;
  private GuiSlider multiplier;
  private GuiSlider degree;
  private String configTrue = ":" + net.minecraft.util.text.TextFormatting.GREEN + " True";
  private String configFalse = ":" + net.minecraft.util.text.TextFormatting.RED + " False";
  private java.text.DecimalFormat format = new java.text.DecimalFormat("#.0");
  
  public ConfigGui() {}
  
  public void func_73863_a(int mouseX, int mouseY, float partialTicks) { func_146276_q_();
    super.func_73863_a(mouseX, mouseY, partialTicks);
  }
  
  public void drawInformation(String line, int mouseX, int mouseY, int startX, int startY, int width, int height) {
    if ((mouseX >= startX) && (mouseX <= startX + width) && (mouseY >= startY) && (mouseY <= startY + height)) {
      func_146279_a(line, mouseX, mouseY);
    }
  }
  
  public void func_73866_w_()
  {
    field_146292_n.clear();
    field_146292_n.add(this.done = new GuiButton(-1, field_146294_l / 2 - 100, field_146295_m - 33, 200, 20, "Done"));
    field_146292_n.add(this.sneaking = new GuiButton(0, field_146294_l / 2 - 100, field_146295_m / 2 - 30, 200, 20, "Requires Sneaking" + (Config.backstabSneaking ? configTrue : configFalse)));
    
    field_146292_n.add(this.itemsall = new GuiButton(1, field_146294_l / 2 - 100, field_146295_m / 2, 200, 20, "All Items Override" + (Config.backstabItemsAll ? configTrue : configFalse)));
    
    field_146292_n.add(this.items = new GuiButton(2, field_146294_l / 2 - 100, field_146295_m / 2 + 30, 95, 20, "Items"));
    field_146292_n.add(this.entities = new GuiButton(3, field_146294_l / 2 + 5, field_146295_m / 2 + 30, 95, 20, "Entities"));
    field_146292_n.add(this.sounds = new GuiButton(4, field_146294_l / 2 - 100, field_146295_m / 2 + 60, 200, 20, "Sounds"));
    field_146292_n.add(this.multiplier = new GuiSlider(5, field_146294_l / 2 - 100, field_146295_m / 2 - 90, 200, 20, "Multplier Damage: ", "", 0.0D, 50.0D, Config.backstabMultiplier, true, true));
    field_146292_n.add(this.degree = new GuiSlider(6, field_146294_l / 2 - 100, field_146295_m / 2 - 60, 200, 20, "Degree: ", "", 0.0D, 360.0D, Config.backstabDegrees, true, true));
    super.func_73866_w_();
  }
  
  public void func_73876_c()
  {
    multiplier.setValue(Double.parseDouble(format.format(multiplier.getValue())));
    degree.setValue(Double.parseDouble(format.format(degree.getValue())));
    Config.backstabMultiplier = multiplier.getValue();
    Config.backstabDegrees = degree.getValue();
    Config.syncFromFields();
  }
  
  public void func_146284_a(GuiButton button) throws java.io.IOException
  {
    switch (field_146127_k) {
    case -1: 
      field_146297_k.func_147108_a(null);
      break;
    case 0: 
      Config.backstabSneaking = !Config.backstabSneaking;
      func_73866_w_();
      break;
    case 1: 
      Config.backstabItemsAll = !Config.backstabItemsAll;
      func_73866_w_();
      break;
    case 2: 
      field_146297_k.func_147108_a(new ItemsConfigGui());
      break;
    case 3: 
      field_146297_k.func_147108_a(new EntitiesConfigGui());
      break;
    case 4: 
      field_146297_k.func_147108_a(new SoundsConfigGui());
      break;
    case 5: 
      multiplier.updateSlider();
      break;
    case 6: 
      degree.updateSlider();
    }
    
    Config.syncFromFields();
    super.func_146284_a(button);
  }
}
