package com.inforno.backstab;

public class Reference
{
  public static final String NAME = "BackStab";
  public static final String VERSION = "1.0.3.1";
  public static final String MODID = "backstab";
  public static final String GUIFACTORY = "com.inforno.backstab.config.ConfigGuiFactory";
  
  public Reference() {}
}
