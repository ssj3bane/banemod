package com.inforno.backstab;

import com.inforno.backstab.config.Config;
import com.inforno.backstab.events.Events;
import com.inforno.backstab.items.Items;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.EventBus;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;






@Mod(name="BackStab", version="1.0.3.1", modid="backstab", guiFactory="com.inforno.backstab.config.ConfigGuiFactory", acceptedMinecraftVersions="[1.12.2]")
public class BackStab
{
  @Mod.Instance("backstab")
  public static BackStab instance;
  
  public BackStab() {}
  
  @Mod.EventHandler
  public void preinit(FMLPreInitializationEvent e)
  {
    Items.preinit();
    Config.preinit();
  }
  
  @Mod.EventHandler
  public void init(FMLInitializationEvent e) {
    MinecraftForge.EVENT_BUS.register(new Events());
  }
  
  @Mod.EventHandler
  @SideOnly(Side.CLIENT)
  public void clientInit(FMLInitializationEvent e) {
    Items.registerRenders();
    Config.clientPreInit();
  }
}
