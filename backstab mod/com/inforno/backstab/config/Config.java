package com.inforno.backstab.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.EventBus;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;







public class Config
{
  public static Configuration config;
  public static final String GENERAL = "general";
  public static double backstabMultiplier;
  public static double backstabDegrees;
  public static boolean backstabSneaking;
  public static final String ITEMS = "items";
  public static String[] backstabItems = new String[0];
  public static boolean backstabItemsAll;
  public static boolean backstabItemsEnabled;
  public static final String ENTITIES = "entities";
  public static String[] backstabEntities = new String[0];
  
  public static final String SOUNDS = "sounds";
  
  public static String backstabSound;
  public static double backstabPitch;
  public static double backstabVolume;
  public static String multiplierString = "Changes the damage multiplier [Default: 5, Range: 0.0-1000.0]";
  public static String degreeString = "Changes the degree needed to backstab [Default: 45, Range: 0.0-360.0]";
  public static String sneakingString = "Requires sneaking to backstab? [Default: false]";
  public static String itemsString = "What items CAN backstab? [Format: modid:itemid]";
  public static String itemsAllString = "Overrides items, will enable everything (including fists) to backstab [Default: false]";
  public static String itemsEnabledString = "Should the daggers be enabled? Warning: Only change before you make a world [Default: true]";
  public static String entitiesString = "What mobs CANNOT be backstab? [Format: modid:entityid]";
  public static String soundsString = "Sound played when the player backstabs [Format: Resource Location or /playsound (sound) format, Default: 'block.anvil.place']";
  public static String pitchString = "Changes the pitch of the backstab sound [Default: 0.4, Range: 0.0-100.0]";
  public static String volumeString = "Changes the volume of the backstab sound [Default: 1.0, Range: 0.0-2.0]";
  
  private static String[] defaultitems = { "backstab:wood_dagger", "backstab:stone_dagger", "backstab:iron_dagger", "backstab:gold_dagger", "backstab:diamond_dagger" };
  private static String[] defaultentities = new String[0];
  public static List<String> items;
  public static List<String> entities;
  
  public Config() {}
  
  public static void preinit() { File configFile = new File(Loader.instance().getConfigDir(), "backstab.cfg");
    config = new Configuration(configFile);
    syncFromFiles();
  }
  
  public static void clientPreInit() {
    MinecraftForge.EVENT_BUS.register(new ConfigEventHandler());
  }
  
  public static Configuration getConfig() {
    return config;
  }
  
  public static void syncFromFiles() {
    syncConfig(true, true);
  }
  
  public static void syncFromGui() {
    syncConfig(false, true);
  }
  
  public static void syncFromFields() {
    syncConfig(false, false);
  }
  
  private static void syncConfig(boolean loadFromConfigFile, boolean readFieldsFromConfig) {
    if (loadFromConfigFile) {
      config.load();
    }
    
    Property propertyBackStabMultiplier = config.get("general", "multiplier", 5.0D, multiplierString, 0.0D, 1000.0D);
    Property propertyBackStabDegrees = config.get("general", "degree", 45.0D, degreeString, 0.0D, 360.0D);
    Property propertyBackStabSneaking = config.get("general", "sneaking", false, sneakingString);
    Property propertyBackStabItems = config.get("items", "items", defaultitems, itemsString);
    Property propertyBackStabItemsAll = config.get("items", "itemsall", false, itemsAllString);
    Property propertyBackStabItemsEnabled = config.get("items", "itemenabled", true, itemsEnabledString);
    Property propertyBackStabEntities = config.get("entities", "entities_blacklist", defaultentities, entitiesString);
    Property propertyBackStabSound = config.get("sounds", "sound", "block.anvil.place", soundsString);
    Property propertyBackStabPitch = config.get("sounds", "pitch", 0.4D, pitchString, 0.0D, 100.0D);
    Property propertyBackStabVolume = config.get("sounds", "volume", 1.0D, volumeString, 0.0D, 2.0D);
    
    List<String> propertyOrderGeneral = new ArrayList();
    propertyOrderGeneral.add(propertyBackStabMultiplier.getName());
    propertyOrderGeneral.add(propertyBackStabDegrees.getName());
    propertyOrderGeneral.add(propertyBackStabSneaking.getName());
    config.setCategoryPropertyOrder("general", propertyOrderGeneral);
    
    List<String> propertyOrderItems = new ArrayList();
    propertyOrderItems.add(propertyBackStabItems.getName());
    propertyOrderItems.add(propertyBackStabItemsAll.getName());
    propertyOrderItems.add(propertyBackStabItemsEnabled.getName());
    config.setCategoryPropertyOrder("items", propertyOrderItems);
    
    List<String> propertyOrderEntities = new ArrayList();
    propertyOrderEntities.add(propertyBackStabEntities.getName());
    config.setCategoryPropertyOrder("entities", propertyOrderEntities);
    
    List<String> propertyOrderSounds = new ArrayList();
    propertyOrderSounds.add(propertyBackStabSound.getName());
    propertyOrderSounds.add(propertyBackStabPitch.getName());
    propertyOrderSounds.add(propertyBackStabVolume.getName());
    config.setCategoryPropertyOrder("sounds", propertyOrderSounds);
    
    if (readFieldsFromConfig) {
      backstabMultiplier = propertyBackStabMultiplier.getDouble();
      backstabDegrees = propertyBackStabDegrees.getDouble();
      backstabSneaking = propertyBackStabSneaking.getBoolean();
      backstabItems = propertyBackStabItems.getStringList();
      backstabItemsAll = propertyBackStabItemsAll.getBoolean();
      backstabItemsEnabled = propertyBackStabItemsEnabled.getBoolean();
      backstabSound = propertyBackStabSound.getString();
      backstabPitch = propertyBackStabPitch.getDouble();
      backstabVolume = propertyBackStabVolume.getDouble();
      items = new ArrayList(Arrays.asList(backstabItems));
      entities = new ArrayList(Arrays.asList(backstabEntities));
    }
    
    backstabItems = (String[])items.toArray(new String[items.size()]);
    backstabEntities = (String[])entities.toArray(new String[entities.size()]);
    
    propertyBackStabMultiplier.set(backstabMultiplier);
    propertyBackStabDegrees.set(backstabDegrees);
    propertyBackStabSneaking.set(backstabSneaking);
    propertyBackStabItems.set(backstabItems);
    propertyBackStabItemsAll.set(backstabItemsAll);
    propertyBackStabItemsEnabled.set(backstabItemsEnabled);
    propertyBackStabEntities.set(backstabEntities);
    propertyBackStabSound.set(backstabSound);
    propertyBackStabPitch.set(backstabPitch);
    propertyBackStabVolume.set(backstabVolume);
    
    if (config.hasChanged())
      config.save();
  }
  
  public static class ConfigEventHandler {
    public ConfigEventHandler() {}
    
    @SubscribeEvent(priority=EventPriority.NORMAL)
    public void onEvent(ConfigChangedEvent.OnConfigChangedEvent event) { if ((event.getModID().equals("backstab")) && (
        (event.getConfigID().equals("general")) || (event.getConfigID().equals("items")) || (event.getConfigID().equals("entities")) || (event.getConfigID().equals("sounds")))) {
        Config.syncFromGui();
      }
    }
  }
}
