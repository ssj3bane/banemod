package com.inforno.backstab.config;

import com.inforno.backstab.gui.MainBackStabGui;
import java.util.Set;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.IModGuiFactory.RuntimeOptionCategoryElement;











public class ConfigGuiFactory
  implements IModGuiFactory
{
  public ConfigGuiFactory() {}
  
  public GuiScreen createConfigGui(GuiScreen parentScreen)
  {
    return new MainBackStabGui();
  }
  
  public boolean hasConfigGui()
  {
    return true;
  }
  


  public void initialize(Minecraft minecraftInstance) {}
  

  public Set<IModGuiFactory.RuntimeOptionCategoryElement> runtimeGuiCategories()
  {
    return null;
  }
}
